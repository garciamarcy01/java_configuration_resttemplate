package com.bancoazteca.api.entity.arcus.impl;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Configuration {
	private static final Logger LOG = LoggerFactory.getLogger(Configuration.class);
	static private final String CONTENT_TYPE = MediaType.APPLICATION_JSON;
	static private final DateTimeFormatter DATE_FORMAT = DateTimeFormatter.ofPattern("EEE, dd MMM yyyy HH:mm:ss O").withLocale(Locale.US);
	static private final ZoneId DATE_ZONE = ZoneId.of("GMT");

	private final String host;
	private final String apiKey;
	private final String secretKey;

	private Boolean useSSL;

	public Configuration(final String host, final String apiKey, final String secretKey) {
		this.host = host;
		this.apiKey = apiKey;
		this.secretKey = secretKey;

		this.useSSL = true;
	}

	public String getDate() {
		LOG.info("GTM Zona Horaria => " + ZoneId.systemDefault());
		return ZonedDateTime.now(ZoneId.systemDefault()).withZoneSameInstant(DATE_ZONE).format(DATE_FORMAT);
	}

	public String getContentType() {
		return CONTENT_TYPE;
	}

	public String getHost() {
		return host;
	}

	public String getApiKey() {
		return apiKey;
	}

	public String getSecretKey() {
		return secretKey;
	}

	public Boolean isUsingSSL() {
		return useSSL;
	}

	public void setUseSSL(final Boolean useSSL) {
		this.useSSL = useSSL;
	}

	public String getAccept() {
		return "application/vnd.regalii.v1.6+json";
	}
}
