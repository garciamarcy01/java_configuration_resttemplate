package com.java.api.entity;

public class Resultado {

	private String name;
	private String balance;
	private String minimum_balance;
	private String currency;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBalance() {
		return balance;
	}

	public void setBalance(String balance) {
		this.balance = balance;
	}

	public String getMinimum_balance() {
		return minimum_balance;
	}

	public void setMinimum_balance(String minimum_balance) {
		this.minimum_balance = minimum_balance;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	@Override
	public String toString() {
		return "Resultado [name=" + name + ", balance=" + balance + ", minimum_balance=" + minimum_balance
				+ ", currency=" + currency + "]";
	}

}
