package com.java.api.entity;

import com.java.api.commons.utils.bean.ResponseTO;

public class ApiResponseTO extends ResponseTO {

	Resultado resultado;

	public Resultado getResultado() {
		return resultado;
	}

	public void setResultado(Resultado resultado) {
		this.resultado = resultado;
	}

}

